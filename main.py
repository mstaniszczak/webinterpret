import argparse

from synchronization.engine import Engine

parser = argparse.ArgumentParser(description='Process products file.')
parser.add_argument(
    '--input-file', type=str, help='an input file name', required=True)
parser.add_argument(
    '--output-file', type=str, help='an output file name', required=True)
parser.add_argument(
    '--threads', type=int, help='an output file name', default=1)

args = parser.parse_args()
print(args)

e = Engine(args.input_file, args.output_file, args.threads)
print("Processing...")
e.run()
print("done.")
