WebInterpret test
=================

The *db* package is a "mini database" code, based on documents in the form of dictionaries. It can that work on dictionaries in the form of trees (each element/document can have many children, and any number of levels). A simple storage based on python dictionaries is added, without the option of persistence (write). It is relatively easy to add storage based, for example, on MongoDB or Redis.
Session class from DB can update whole branches in the database on a multithreaded basis.

Requirements
------------
* Python 3.4+
* tl.testing - for run whole tests. Without it, only tests for not threaded part will be executed.

Hot to run program
------------------

$ python main.py --input-file input_file_name --output-file output_file_name

Optional parameters:
 * --threads - number of threads which can be used

Answers for additional questions
--------------------------------

* Q: Describe how would you implement asynchronous processing in multiple threads/processes of input events (in contrast to processing one by one).
* A: In some part, multithreading is implemented in this solution. Here, the update of the entire product tree is launched on a multithreaded basis. After expanding the system of locks, for example on your own dedicated thread safe queue, you can try to fire all updates and inserts at once on threads. Lock, however, must ensure not to allow to update at the same time the same product, e.g. by creating queues per product.

* Q: How would you design architecture for system supporting synchronization of hundreds of millions of items in near real time? How would you deploy your solution, what components (like databases, or cache) would be required in your architecture?
* A: Best will be to use NoSQL DB, not cutom solution as in presented code. But it can be done for example with MongoDB for store product. Even one document per whole product tree + indexes (also in MongoDB, or for example in Redis) for fast query "root product" by child id. One document for whole tree will ensure cohesion of product, MongoDB should slow in this case situation woith multi updates on one product in this same time. API application can be deployed on multi servers + MongoDB with Redis also can be set on multi servers on cluster.
