from db.exceptions import LockIsAlreadySetException, LockIsNotSetException


class BaseLocks:
    def set(self, oid):
        """ Set access lock to object.

        :param oid: object identifier
        """
        raise NotImplementedError()

    def remove(self, oid):
        """ Remove access lock to object.

        :param oid: object identifier
        """
        raise NotImplementedError()

    def check(self, oid):
        """ Checks whether the object has a access lock set.

        :param oid: object identifier
        :return: boolean
        """
        raise NotImplementedError()


class Locks:
    """ Locks objects. """

    def __init__(self, locks: BaseLocks):
        """ Initialization.

        :param locks: locks object
        """
        self.locks = locks

    def set(self, oid):
        """ Set access lock to object.

        :param oid: object identifier
        """
        self.locks.set(oid)

    def remove(self, oid):
        """ Remove access lock to object.

        :param oid: object identifier
        """
        self.locks.remove(oid)

    def check(self, oid):
        """ Checks whether the object has a access lock set.

        :param oid: object identifier
        :return: boolean
        """
        return self.locks.check(oid)


class ListLocks(BaseLocks):
    """ Basic lock object -list based."""

    def __init__(self):
        """ Initialization. """
        self.__locks = []

    def set(self, oid):
        """ Set access lock to object.

        :param oid: object identifier
        """
        if self.check(oid):
            raise LockIsAlreadySetException(
                'Lock for object {oid} is already set'.format(oid=oid))

        self.__locks.append(oid)

    def remove(self, oid):
        """ Remove access lock to object.

        :param oid: object identifier
        """
        if not self.check(oid):
            raise LockIsNotSetException(
                'Lock for object {oid} isn\'t set'.format(oid=oid))
        self.__locks.remove(oid)

    def check(self, oid):
        """ Checks whether the object has a access lock set.

        :param oid: object identifier
        :return: boolean
        """
        return oid in self.__locks
