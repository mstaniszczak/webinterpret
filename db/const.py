STORAGE_ID_KEY = '__id'
STORAGE_ROOT_KEY = '__root'
STORAGE_PARENT_KEY = '__parent'
STORAGE_CHILDS_KEY = '__childs'
STORAGE_POSTERITY_KEY = '__posterity'

ACTION_DELETE = 'delete'
ACTION_UPDATE = 'update'

EVENT_TYPE_UPDATE = 'update'
EVENT_TYPE_CREATE = 'create'
EVENT_TYPE_DELETE = 'delete'
EVENT_TYPE_RETRIEVE = 'retrieve'
