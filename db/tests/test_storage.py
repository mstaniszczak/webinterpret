import unittest

from db.const import STORAGE_ID_KEY, STORAGE_PARENT_KEY, STORAGE_CHILDS_KEY, \
    STORAGE_ROOT_KEY, STORAGE_POSTERITY_KEY
from db.exceptions import ConstraintException, AlreadyExistsException, \
    NotExistsException
from db.storage import Storage, DictStorage, BaseStorage


class TestStorage(unittest.TestCase):
    def setUp(self):
        data_storage = DictStorage()
        self.storage = Storage(data_storage)

    def test_create_ok(self):
        # create object from dict
        data = {
            STORAGE_ID_KEY: 321,
            'foo': 'bar',
            'count': 123
        }
        oid = self.storage.create(**data)
        self.assertEqual(oid, 321)

        # create object from parameters
        oid = self.storage.create(__id=1, __parent=321, bar='foo')
        self.assertEqual(oid, 1)

        # no id given
        oid = self.storage.create(__parent=321, bar='foo')
        self.assertEqual(str(oid).count('-'), 4)

    def test_create_constraint_exception(self):
        # incorrect parent id
        with self.assertRaises(ConstraintException):
            self.storage.create(__parent=123, bar='foo')

    def test_create_constraint_alreade_exists_exception(self):
        self.storage.create(__id=1, bar='foo')
        # incorrect parent id
        with self.assertRaises(AlreadyExistsException):
            self.storage.create(__id=1, bar='foo')

    def test_get_ok(self):
        data = {
            STORAGE_ID_KEY: 321,
            'foo': 'bar',
            'count': 123
        }
        oid = self.storage.create(**data)
        extracted = self.storage.get(oid)
        self.assertEqual(extracted[STORAGE_ID_KEY], data[STORAGE_ID_KEY])
        self.assertEqual(extracted['count'], data['count'])
        self.assertEqual(extracted['foo'], data['foo'])
        self.assertNotIn(STORAGE_PARENT_KEY, extracted)
        self.assertIsNone(extracted[STORAGE_CHILDS_KEY])

    def test_get_ok_parents_childs(self):
        self.storage.create(__id=1, foo='bar.1')
        self.storage.create(__id=2, __parent=1, foo='bar.1.2')
        self.storage.create(__id=3, __parent=1, foo='bar.1.3')
        self.storage.create(__id=4, __parent=2, foo='bar.1.2.4')

        # test object #1
        extracted = self.storage.get(oid=1)
        self.assertEqual(extracted[STORAGE_ID_KEY], 1)
        self.assertEqual(extracted['foo'], 'bar.1')
        self.assertEqual(extracted[STORAGE_CHILDS_KEY], [2, 3])
        self.assertNotIn(STORAGE_PARENT_KEY, extracted)
        self.assertNotIn(STORAGE_ROOT_KEY, extracted)

        # test object #2
        extracted = self.storage.get(oid=2)
        self.assertEqual(extracted[STORAGE_ID_KEY], 2)
        self.assertEqual(extracted['foo'], 'bar.1.2')
        self.assertEqual(extracted[STORAGE_CHILDS_KEY], [4])
        self.assertEqual(extracted[STORAGE_PARENT_KEY], 1)
        self.assertEqual(extracted[STORAGE_ROOT_KEY], 1)

        # test object #3
        extracted = self.storage.get(oid=3)
        self.assertEqual(extracted[STORAGE_ID_KEY], 3)
        self.assertEqual(extracted['foo'], 'bar.1.3')
        self.assertIsNone(extracted[STORAGE_CHILDS_KEY])
        self.assertEqual(extracted[STORAGE_PARENT_KEY], 1)
        self.assertEqual(extracted[STORAGE_ROOT_KEY], 1)

        # test object #4
        extracted = self.storage.get(oid=4)
        self.assertEqual(extracted[STORAGE_ID_KEY], 4)
        self.assertEqual(extracted['foo'], 'bar.1.2.4')
        self.assertIsNone(extracted[STORAGE_CHILDS_KEY])
        self.assertEqual(extracted[STORAGE_PARENT_KEY], 2)
        self.assertEqual(extracted[STORAGE_ROOT_KEY], 1)

    def test_get_posterity(self):
        self.storage.create(__id=1, foo='p.1')
        self.storage.create(__id=12, __parent=1, foo='p.1.2')
        self.storage.create(__id=124, __parent=12, foo='p.1.2.4')
        self.storage.create(__id=125, __parent=12, foo='p.1.2.5')
        self.storage.create(__id=13, __parent=1, foo='p.1.3')
        self.storage.create(__id=136, __parent=13, foo='p.1.3.6')
        self.storage.create(__id=137, __parent=13, foo='p.1.3.7')
        self.storage.create(__id=1378, __parent=137, foo='p.1.3.7.8')
        self.storage.create(__id=13789, __parent=1378, foo='p.1.3.7.8.9')
        self.storage.create(__id=137810, __parent=1378, foo='p.1.3.7.8.10')

        self.storage.create(__id='a1', foo='a.1')
        self.storage.create(__id='a12', __parent='a1', foo='a.1.2')
        self.storage.create(__id='a13', __parent='a1', foo='a.1.3')
        self.storage.create(__id='a124', __parent='a12', foo='a.1.2.4')
        self.storage.create(__id='a125', __parent='a12', foo='a.1.2.5')

        ids = [12, 124, 125, 13, 136, 137, 1378, 13789, 137810]
        data = self.storage.get(1)
        self.assertNotIn(STORAGE_ROOT_KEY, data)
        self.assertEqual(data[STORAGE_POSTERITY_KEY], ids)

        for oid in ids:
            data = self.storage.get(oid)
            self.assertEqual(data[STORAGE_ROOT_KEY], 1)
            self.assertNotIn(STORAGE_POSTERITY_KEY, data)

        ids = ['a12', 'a13', 'a124', 'a125']
        data = self.storage.get('a1')
        self.assertNotIn(STORAGE_ROOT_KEY, data)
        self.assertEqual(data[STORAGE_POSTERITY_KEY], ids)

        for oid in ids:
            data = self.storage.get(oid)
            self.assertEqual(data[STORAGE_ROOT_KEY], 'a1')
            self.assertNotIn(STORAGE_POSTERITY_KEY, data)


    def test_get_ok_not_exists(self):
        extracted = self.storage.get(123)
        self.assertIsNone(extracted)

    def test_find(self):
        self.storage.create(__id=1, bar='foo.1')
        self.storage.create(__id=2, __parent=1, bar='foo.1.2')
        results = self.storage.find([1])
        self.assertIn(1, results)

        result = results[1]
        self.assertEqual(result[STORAGE_ID_KEY], 1)
        self.assertEqual(result['bar'], 'foo.1')
        self.assertEqual(result[STORAGE_CHILDS_KEY], [2])
        self.assertNotIn(STORAGE_PARENT_KEY, result)

        results = self.storage.find([1, 2])
        self.assertIn(1, results)
        self.assertIn(2, results)
        result = results[1]
        self.assertEqual(result[STORAGE_ID_KEY], 1)
        self.assertEqual(result['bar'], 'foo.1')
        self.assertEqual(result[STORAGE_CHILDS_KEY], [2])
        self.assertNotIn(STORAGE_PARENT_KEY, result)
        result = results[2]
        self.assertEqual(result[STORAGE_ID_KEY], 2)
        self.assertEqual(result['bar'], 'foo.1.2')
        self.assertIsNone(result[STORAGE_CHILDS_KEY])
        self.assertEqual(result[STORAGE_PARENT_KEY], 1)

    def test_find_not_exists(self):
        results = self.storage.find([1])
        self.assertEqual(results, {})

        self.storage.create(__id=1, bar='foo.1')
        self.storage.create(__id=2, __parent=1, bar='foo.1.2')

        results = self.storage.find([1, 3])
        self.assertIn(1, results)
        self.assertNotIn(3, results)

        result = results[1]
        self.assertEqual(result[STORAGE_ID_KEY], 1)
        self.assertEqual(result['bar'], 'foo.1')
        self.assertEqual(result[STORAGE_CHILDS_KEY], [2])
        self.assertNotIn(STORAGE_PARENT_KEY, result)

    def test_remove_ok(self):
        self.storage.create(__id=1, bar='foo.1')
        self.storage.create(__id=2, __parent=1, bar='foo.1.2')

        result = self.storage.get(1)
        self.assertEqual(result[STORAGE_POSTERITY_KEY], [2])
        self.assertEqual(result[STORAGE_CHILDS_KEY], [2])
        result = self.storage.get(2)
        self.assertIsNotNone(result)

        self.storage.delete(2)
        result = self.storage.get(1)
        self.assertEqual(result[STORAGE_POSTERITY_KEY], [])
        self.assertEqual(result[STORAGE_CHILDS_KEY], [])
        result = self.storage.get(2)
        self.assertIsNone(result)

    def test_remove_children(self):
        self.storage.create(__id='p1', bar='p.1')
        self.storage.create(__id='p12', __parent='p1', bar='p.1.2')
        self.storage.create(__id='p13', __parent='p1', bar='p.1.3')
        self.storage.create(__id='p134', __parent='p13', bar='p.1.3.4')
        self.storage.create(__id='p135', __parent='p13', bar='p.1.3.5')
        self.storage.create(__id='p1356', __parent='p135', bar='p.1.3.5.6')

        result = self.storage.get('p13')
        self.assertEquals(result[STORAGE_PARENT_KEY], 'p1')
        self.assertEquals(result[STORAGE_CHILDS_KEY], ['p134', 'p135'])
        result = self.storage.get('p135')
        self.assertEquals(result[STORAGE_PARENT_KEY], 'p13')
        self.assertEquals(result[STORAGE_CHILDS_KEY], ['p1356'])
        result = self.storage.get('p1356')
        self.assertEquals(result[STORAGE_PARENT_KEY], 'p135')
        self.assertEquals(result[STORAGE_CHILDS_KEY], None)

        self.storage.delete('p13')
        result = self.storage.get('p13')
        self.assertIsNone(result)
        result = self.storage.get('p134')
        self.assertIsNotNone(result)
        result = self.storage.get('p135')
        self.assertIsNotNone(result)
        result = self.storage.get('p1356')
        self.assertIsNotNone(result)

    def test_remove_exception(self):
        with self.assertRaises(NotExistsException):
            self.storage.delete(123)

    def test_update(self):
        self.storage.create(__id=1, foo='bar.1')

        extracted = self.storage.get(oid=1)
        self.assertEqual(extracted[STORAGE_ID_KEY], 1)
        self.assertEqual(extracted['foo'], 'bar.1')
        self.assertIsNone(extracted[STORAGE_CHILDS_KEY])
        self.assertNotIn(STORAGE_PARENT_KEY, extracted)
        self.assertNotIn(STORAGE_ROOT_KEY, extracted)

        self.storage.update(1, foo='foo')
        extracted = self.storage.get(oid=1)
        self.assertEqual(extracted[STORAGE_ID_KEY], 1)
        self.assertEqual(extracted['foo'], 'foo')
        self.assertIsNone(extracted[STORAGE_CHILDS_KEY])
        self.assertNotIn(STORAGE_PARENT_KEY, extracted)
        self.assertNotIn(STORAGE_ROOT_KEY, extracted)

    def test_update_move(self):
        self.storage.create(__id='p1', bar='p.1')
        self.storage.create(__id='p12', __parent='p1', bar='p.1.2')
        self.storage.create(__id='p13', __parent='p1', bar='p.1.3')
        self.storage.create(__id='p134', __parent='p13', bar='p.1.3.4')
        self.storage.create(__id='p135', __parent='p13', bar='p.1.3.5')

        posterity_ids = ['p12', 'p13', 'p134', 'p135']
        result = self.storage.get('p1')
        self.assertEquals(
            sorted(result[STORAGE_POSTERITY_KEY]),
            sorted(posterity_ids))
        self.assertEquals(result[STORAGE_CHILDS_KEY], ['p12', 'p13'])

        result = self.storage.get('p13')
        self.assertEquals(result[STORAGE_PARENT_KEY], 'p1')
        self.assertEquals(result[STORAGE_CHILDS_KEY], ['p134', 'p135'])

        self.storage.update('p13', __parent='p12', moved='true')

        result = self.storage.get('p1')
        self.assertEquals(
            sorted(result[STORAGE_POSTERITY_KEY]),
            sorted(posterity_ids))
        self.assertEquals(result[STORAGE_CHILDS_KEY], ['p12'])

        result = self.storage.get('p13')
        self.assertEquals(result[STORAGE_PARENT_KEY], 'p12')
        self.assertEquals(result[STORAGE_CHILDS_KEY], ['p134', 'p135'])
        self.assertEquals(result['moved'], 'true')

    def test_update_exception(self):
        with self.assertRaises(NotExistsException):
            self.storage.update(123, foo='bar')

    def test_list_roots(self):
        self.storage.create(__id='p1', bar='p.1')
        self.storage.create(__id='p12', __parent='p1', bar='p.1.2')
        self.storage.create(__id='p13', __parent='p1', bar='p.1.3')
        self.storage.create(__id='p134', __parent='p13', bar='p.1.3.4')
        self.storage.create(__id='p135', __parent='p13', bar='p.1.3.5')

        self.storage.create(__id='a1', bar='a.1')
        self.storage.create(__id='b1', bar='b.1')

        result = sorted(self.storage.list_roots())
        ids = sorted(['p1', 'a1', 'b1'])
        self.assertEquals(result, ids)


class TestDictStorage(TestStorage):
    def setUp(self):
        self.storage = DictStorage()


class TestBaseStorage(unittest.TestCase):
    def setUp(self):
        self.storage = BaseStorage()

    def test_get(self):
        with self.assertRaises(NotImplementedError):
            self.storage.get(1)

    def test_find(self):
        with self.assertRaises(NotImplementedError):
            self.storage.find([1])

    def test_create(self):
        with self.assertRaises(NotImplementedError):
            self.storage.create(__id=1, foo='bar')

    def test_update(self):
        with self.assertRaises(NotImplementedError):
            self.storage.update(1, foo='bar')

    def test_delete(self):
        with self.assertRaises(NotImplementedError):
            self.storage.delete(1)

    def test_list_roots(self):
        with self.assertRaises(NotImplementedError):
            self.storage.list_roots()
