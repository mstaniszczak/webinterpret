import unittest

from db.exceptions import LockIsAlreadySetException, LockIsNotSetException
from db.lock import Locks, ListLocks, BaseLocks


class TestLocks(unittest.TestCase):
    def setUp(self):
        self.locks = Locks(ListLocks())

    def test_set(self):
        self.locks.set(1)
        self.locks.set(3)
        self.locks.set(5)

        self.assertTrue(self.locks.check(1))
        self.assertFalse(self.locks.check(2))
        self.assertTrue(self.locks.check(3))
        self.assertFalse(self.locks.check(4))
        self.assertTrue(self.locks.check(5))

    def test_set_exception(self):
        self.locks.set(1)
        with self.assertRaises(LockIsAlreadySetException):
            self.locks.set(1)

    def test_remove(self):
        self.locks.set(1)
        self.locks.set(3)
        self.locks.set(5)

        self.assertTrue(self.locks.check(1))
        self.assertFalse(self.locks.check(2))
        self.assertTrue(self.locks.check(3))
        self.assertFalse(self.locks.check(4))
        self.assertTrue(self.locks.check(5))

        self.locks.remove(1)
        self.locks.remove(3)
        self.locks.remove(5)

        self.assertFalse(self.locks.check(1))
        self.assertFalse(self.locks.check(2))
        self.assertFalse(self.locks.check(3))
        self.assertFalse(self.locks.check(4))
        self.assertFalse(self.locks.check(5))

    def test_remove_exception(self):
        with self.assertRaises(LockIsNotSetException):
            self.locks.remove(1)

class TestListLocks(TestLocks):
    def setUp(self):
        self.locks = ListLocks()

class TestBaseLocks(unittest.TestCase):
    def setUp(self):
        self.locks = BaseLocks()

    def test_set(self):
        with self.assertRaises(NotImplementedError):
            self.locks.set(1)

    def test_check(self):
        with self.assertRaises(NotImplementedError):
            self.locks.check(1)

    def test_remove(self):
        with self.assertRaises(NotImplementedError):
            self.locks.remove(1)
