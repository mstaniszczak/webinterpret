import unittest
from unittest.mock import MagicMock, call

try:
    from tl.testing.thread import ThreadJoiner
    thread_tests = True
except ImportError:
    thread_tests = False

from db.const import STORAGE_POSTERITY_KEY, STORAGE_CHILDS_KEY, \
    STORAGE_PARENT_KEY, STORAGE_ROOT_KEY, EVENT_TYPE_DELETE, EVENT_TYPE_CREATE, \
    EVENT_TYPE_RETRIEVE, EVENT_TYPE_UPDATE, STORAGE_ID_KEY
from db.exceptions import UpdateException, DeleteException
from db.lock import Locks, ListLocks
from db.session import DBSession
from db.storage import Storage, DictStorage


class TestStorage(unittest.TestCase):
    def setUp(self):
        locks = Locks(ListLocks())
        self.session_nt = DBSession(
            locks=locks,
            storage=Storage(DictStorage()))

        self.session_t = DBSession(
            locks=locks,
            storage=Storage(DictStorage()),
            threads=2)

    def _generate_test_data(self, threading=False):
        session = self.session_t if threading else self.session_nt

        session.create(__id='a1', value='a.1')
        session.create(__id='a12', __parent='a1', value='a.1.2')
        session.create(__id='a13', __parent='a1', value='a.1.3')
        session.create(__id='a134', __parent='a13', value='a.1.3.4')
        session.create(__id='a135', __parent='a13', value='a.1.3.5')

        session.create(__id='b1', value='b.1')
        session.create(__id='b12', __parent='b1', value='b.1.2')
        session.create(__id='b124', __parent='b12', value='b.1.2.4')
        session.create(__id='b1245', __parent='b124', value='b.1.2.4.5')
        session.create(__id='b1246', __parent='b124', value='b.1.2.4.6')
        session.create(__id='b13', __parent='b1', value='b.1.3')

    def test_list_roots(self):
        self._generate_test_data()
        session = self.session_nt
        self.assertEqual(sorted(session.list_roots()), ['a1', 'b1'])

    def test_get(self):
        self._generate_test_data()
        session = self.session_nt

        result = session.get('a1')
        posterity_ids = ['a12', 'a13', 'a134', 'a135']
        childs_ids = ['a12', 'a13']
        self.assertEqual(
            sorted(result[STORAGE_POSTERITY_KEY]), sorted(posterity_ids))
        self.assertEqual(
            sorted(result[STORAGE_CHILDS_KEY]), sorted(childs_ids))
        self.assertEqual(result['value'], 'a.1')
        self.assertNotIn(STORAGE_ROOT_KEY, result)

        result = session.get('a135')
        self.assertNotIn(STORAGE_POSTERITY_KEY, result)
        self.assertIsNone(result[STORAGE_CHILDS_KEY])
        self.assertEqual(result['value'], 'a.1.3.5')
        self.assertEqual(result[STORAGE_PARENT_KEY], 'a13')
        self.assertEqual(result[STORAGE_ROOT_KEY], 'a1')

    def test_get_incorrect_id(self):
        self._generate_test_data()
        session = self.session_nt

        result = session.get('a123456')
        self.assertIsNone(result)

    def test_find(self):
        self._generate_test_data()
        session = self.session_nt

        result = session.find(['a1', 'a134', 'b12'])
        keys = ['a134', 'b12', 'a1']
        self.assertEqual(sorted(result.keys()), sorted(keys))

        self.assertEqual(result['a1'], session.get('a1'))
        self.assertEqual(result['b12'], session.get('b12'))
        self.assertEqual(result['a134'], session.get('a134'))

    def test_find_incorrect_id(self):
        self._generate_test_data()
        session = self.session_nt

        result = session.find(['a1234567', 'a134', 'b12'])
        keys = ['a134', 'b12']
        self.assertEqual(sorted(result.keys()), sorted(keys))

        self.assertNotIn('a1234567', result)
        self.assertEqual(result['b12'], session.get('b12'))
        self.assertEqual(result['a134'], session.get('a134'))

    def test_create(self):
        session = self.session_nt

        result1 = session.create(__id='a1', value='a.1')
        result2 = session.create(__id='a12', __parent='a1', value='a.1.2')

        self.assertEqual(result1, 'a1')
        self.assertEqual(result2, 'a12')

        result1 = session.get('a1')
        result2 = session.get('a12')

        self.assertEqual(result1[STORAGE_CHILDS_KEY], ['a12'])
        self.assertEqual(result1[STORAGE_POSTERITY_KEY], ['a12'])
        self.assertNotIn(STORAGE_ROOT_KEY, result1)

        self.assertEqual(result2[STORAGE_ROOT_KEY], 'a1')
        self.assertNotIn(STORAGE_POSTERITY_KEY, result2)
        self.assertIsNone(result2[STORAGE_CHILDS_KEY])

    def test_update_no_threads_1(self):
        self._generate_test_data()
        session = self.session_nt

        result1 = session.get('a1')
        self.assertEqual(result1['value'], 'a.1')
        result1 = session.get('a12')
        self.assertEqual(result1['value'], 'a.1.2')

        session.update('a1', value="foobar")
        result = session.get('a1')
        self.assertEqual(result['value'], 'foobar')
        result = session.get('a12')
        self.assertEqual(result['value'], 'a.1.2')

    def test_update_no_threads_2(self):
        self._generate_test_data()
        session = self.session_nt

        result1 = session.get('a1')
        self.assertEqual(result1['value'], 'a.1')
        result1 = session.get('a12')
        self.assertEqual(result1['value'], 'a.1.2')

        session.update('a1', populate=True, value="foobar")
        result = session.get('a1')
        self.assertEqual(result['value'], 'foobar')
        result = session.get('a12')
        self.assertEqual(result['value'], 'foobar')
        result = session.get('a13')
        self.assertEqual(result['value'], 'foobar')

    def test_update_no_threads_3(self):
        self._generate_test_data()
        session = self.session_nt

        result1 = session.get('a1')
        self.assertEqual(result1['value'], 'a.1')
        result1 = session.get('a12')
        self.assertEqual(result1['value'], 'a.1.2')

        def pupulate_test(data, new_data):
            if data[STORAGE_ID_KEY] == 'a1':
                return False
            return True

        session.update('a1', populate=pupulate_test, value="foobar")
        result = session.get('a1')
        self.assertEqual(result['value'], 'a.1')
        result = session.get('a12')
        self.assertEqual(result['value'], 'foobar')
        result = session.get('a13')
        self.assertEqual(result['value'], 'foobar')

    def test_update_threads_1(self):
        if not thread_tests:
            print("For thread tests tl.testing is required")
            return

        self._generate_test_data(True)
        session = self.session_t

        result1 = session.get('a1')
        self.assertEqual(result1['value'], 'a.1')
        result1 = session.get('a12')
        self.assertEqual(result1['value'], 'a.1.2')

        session.update('a1', value="foobar")
        result = session.get('a1')
        self.assertEqual(result['value'], 'foobar')
        result = session.get('a12')
        self.assertEqual(result['value'], 'a.1.2')

    def test_update_threads_2(self):
        if not thread_tests:
            print("For thread tests tl.testing is required")
            return

        self._generate_test_data(True)
        session = self.session_t

        result1 = session.get('a1')
        self.assertEqual(result1['value'], 'a.1')
        result1 = session.get('a12')
        self.assertEqual(result1['value'], 'a.1.2')

        with ThreadJoiner(1):
            session.update('a1', populate=True, value="foobar")

        result = session.get('a1')
        self.assertEqual(result['value'], 'foobar')
        result = session.get('a12')
        self.assertEqual(result['value'], 'foobar')

    def test_update_threads_3(self):
        if not thread_tests:
            print("For thread tests tl.testing is required")
            return

        self._generate_test_data(True)
        session = self.session_t

        result1 = session.get('a1')
        self.assertEqual(result1['value'], 'a.1')
        result1 = session.get('a12')
        self.assertEqual(result1['value'], 'a.1.2')

        with ThreadJoiner(1):
            def pupulate_test(data, new_data):
                if data[STORAGE_ID_KEY] == 'a1':
                    return False
                return {'value': '{} foo'.format(data['value'])}

            session.update('a1', populate=pupulate_test, value="foobar")

        result = session.get('a1')
        self.assertEqual(result['value'], 'a.1')
        result = session.get('a12')
        self.assertEqual(result['value'], 'a.1.2 foo')

    def test_update_not_exists_no_thread(self):
        self._generate_test_data()
        session = self.session_nt

        with self.assertRaises(UpdateException):
            session.update('a132111', populate=True, value="foobar")

    def test_update_not_exists_threads(self):
        if not thread_tests:
            print("For thread tests tl.testing is required")
            return

        self._generate_test_data(True)
        session = self.session_t

        with ThreadJoiner(1):
            with self.assertRaises(UpdateException):
                session.update('a132111', populate=True, value="foobar")

    def test_delete_no_threads(self):
        self._generate_test_data()
        session = self.session_nt

        ids = ['a13', 'a134', 'a135']
        result = session.find(ids)
        self.assertEqual(sorted(ids), sorted(result.keys()))
        session.delete('a13')

        result = session.find(ids)
        self.assertEqual(len(result), 0)

    def test_delete_threads(self):
        if not thread_tests:
            print("For thread tests tl.testing is required")
            return

        self._generate_test_data(True)
        session = self.session_t

        ids = ['a13', 'a134', 'a135']
        result = session.find(ids)
        self.assertEqual(sorted(ids), sorted(result.keys()))
        with ThreadJoiner(1):
            session.delete('a13')

        result = session.find(ids)
        self.assertEqual(len(result), 0)

    def test_delete_incorrect_object_no_threads(self):
        self._generate_test_data()
        session = self.session_nt

        with self.assertRaises(DeleteException):
            session.delete('a1234567')

    def test_delete_incorrect_object_threads(self):
        self._generate_test_data(True)
        session = self.session_t

        with self.assertRaises(DeleteException):
            session.delete('a1234567')

    def test_delete_event_no_thread(self):
        self._generate_test_data()
        session = self.session_nt

        event_method = MagicMock()
        session.add_event(EVENT_TYPE_DELETE, event_method)
        session.delete('a13')

        event_method.assert_has_calls([
            call(oid='a13'), call(oid='a134'), call(oid='a135')],
            any_order=True)

    def test_delete_event_thread(self):
        self._generate_test_data(True)
        session = self.session_t

        event_method = MagicMock()
        session.add_event(EVENT_TYPE_DELETE, event_method)
        with ThreadJoiner(1):
            session.delete('a13')

        event_method.assert_has_calls([
            call(oid='a13'), call(oid='a134'), call(oid='a135')],
            any_order=True)

    def test_create_event(self):
        session = self.session_nt

        event_method = MagicMock()
        session.add_event(EVENT_TYPE_CREATE, event_method)
        session.create(__id='a1', value='a.1')
        session.create(__id='a12', __parent='a1', value='a.1.2')

        event_method.assert_has_calls([
            call(__id='a1', value='a.1'),
            call(__id='a12', __parent='a1', value='a.1.2')],
            any_order=True)

    def test_retrieve_event(self):
        self._generate_test_data()
        session = self.session_nt

        event_method = MagicMock()
        session.add_event(EVENT_TYPE_RETRIEVE, event_method)
        session.find(['a1', 'a134', 'b12'])

        event_method.assert_has_calls([call(ids=['a1', 'a134', 'b12'])],
            any_order=True)

        event_method.reset_mock()

        session.get('a1')
        session.get('a134')
        session.get('b12')
        event_method.assert_has_calls([
            call(oid='a1'), call(oid='a134'), call(oid='b12')],
            any_order=True)

    def test_update_event_no_threads(self):
        self._generate_test_data()
        session = self.session_nt

        event_method = MagicMock()
        session.add_event(EVENT_TYPE_UPDATE, event_method)
        session.update('a1', populate=True, value="foobar")

        event_method.assert_has_calls([
            call(oid='a12', value='foobar'),
            call(oid='a13', value='foobar'),
            call(oid='a134', value='foobar'),
            call(oid='a135', value='foobar')],
            any_order=True)

    def test_update_event_thread(self):
        self._generate_test_data(True)
        session = self.session_t

        event_method = MagicMock()
        session.add_event(EVENT_TYPE_UPDATE, event_method)
        with ThreadJoiner(1):
            session.update('a1', populate=True, value="foobar")

        event_method.assert_has_calls([
            call(oid='a12', value='foobar'),
            call(oid='a13', value='foobar'),
            call(oid='a134', value='foobar'),
            call(oid='a135', value='foobar')],
            any_order=True)
