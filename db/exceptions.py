class DBExceptions(Exception):
    pass


class NotExistsException(DBExceptions):
    pass


class AlreadyExistsException(DBExceptions):
    pass


class ConstraintException(DBExceptions):
    pass


class LockException(DBExceptions):
    pass


class LockIsAlreadySetException(LockException):
    pass


class LockIsNotSetException(LockException):
    pass


class DeleteException(DBExceptions):
    pass


class UpdateException(DBExceptions):
    pass


class AddingEventException(DBExceptions):
    pass
