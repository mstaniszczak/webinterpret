import logging
import uuid

from db.const import STORAGE_PARENT_KEY, STORAGE_ID_KEY, STORAGE_CHILDS_KEY, \
    STORAGE_POSTERITY_KEY, STORAGE_ROOT_KEY
from db.exceptions import ConstraintException, AlreadyExistsException, \
    NotExistsException


logger = logging.getLogger(__name__)


class BaseStorage:
    """ Base storage class """

    def list_roots(self):
        """ Get ids of all roots objects."""
        raise NotImplementedError()

    def get(self, oid):
        """ Extracting the object from the database.

        :param oid: object identifier
        :return: dictoinry with object or None
        """
        raise NotImplementedError()

    def find(self, ids: list):
        """ Looking for more objects by list.

        :param ids: identifiers of objects as a list
        :return: dictionary of object dictionaries. Keys are identifiers.
        """
        raise NotImplementedError()

    def create(self, **kwargs):
        """ Create new object.

        You have to give ID in __id key. If you miss it, OID will be generated.
        You can add __parent attribute, to determinate parent object of this
        object,

        :param kwargs: object fields.
        :return: OID of the just created object
        """
        raise NotImplementedError()

    def update(self, oid, **kwargs):
        """ Update object.

        If you give __parent attribute, and it will be another than old parent,
        it will move object to new parent and delete it from old one.

        :param oid: object identifier.
        """
        raise NotImplementedError()

    def delete(self, oid):
        """ Delete object.

        If you delete object which have child, it will delete also his child
        objects.

        :param oid: object identifier.
        """
        raise NotImplementedError()


class Storage:
    """ Storage object. """

    def __init__(self, storage: BaseStorage):
        """ Initialization of storage.

        :param storage: selected storage object.
        """
        self.storage = storage

    def list_roots(self):
        """ Get ids of all roots objects."""
        return self.storage.list_roots()

    def get(self, oid):
        """ Extracting the object from the database.

        :param oid: object identifier
        :return: dictoinry with object or None
        """
        return self.storage.get(oid)

    def find(self, ids: list):
        """ Looking for more objects by list.

        :param ids: identifiers of objects as a list
        :return: dictionary of object dictionaries. Keys are identifiers.
        """
        return self.storage.find(ids)

    def create(self, **kwargs):
        """ Create new object.

        You have to give ID in __id key. If you miss it, OID will be generated.
        You can add __parent attribute, to determinate parent object of this
        object,

        :param kwargs: object fields.
        :return: OID of the just created object
        """

        return self.storage.create(**kwargs)

    def update(self, oid, **kwargs):
        """ Update object.

        If you give __parent attribute, and it will be another than old parent,
        it will move object to new parent and delete it from old one.

        :param oid: object identifier.
        """
        return self.storage.update(oid, **kwargs)

    def delete(self, oid):
        """ Update object.

        If you give __parent attribute, and it will be another than old parent,
        it will move object to new parent and delete it from old one.

        :param oid: object identifier.
        """
        return self.storage.delete(oid)


class DictStorage(BaseStorage):
    """ Simple storage engine, based on dictionary.

    .. note::
    It's not permanently storage - not store data on any file system. All data
    will be looses after restart.
    """

    def __init__(self):
        """ Initialization.

        Mainly set initial state of indexes.
        """
        self.__data = {}  # data storage
        self.__roots_of_objects = {}  # index: objects' roots ids
        self.__parents = {}  # index: parents of objects
        self.__childs = {}  # index: childs of objects
        self.__roots_posterity = {}  # index: all roots posterity

    def __set_root(self, oid):
        """Set OID of root object."""
        last_parent_oid = parent_oid = self.__parents.get(oid)
        while parent_oid:
            last_parent_oid = parent_oid
            parent_oid = self.__parents.get(parent_oid)
        self.__roots_of_objects[oid] = last_parent_oid
        return last_parent_oid

    def __update_root_posterity(self, posterity_oid, root_oid):
        """Update posterity of root object."""
        if root_oid not in self.__roots_posterity:
            self.__roots_posterity[root_oid] = []
        self.__roots_posterity[root_oid].append(posterity_oid)

    def __set_parent(self, oid, parent_oid):
        """ Set parent object,"""
        if parent_oid not in self.__data:
            raise ConstraintException(
                'Parent object ({id}) not exists'.format(id=parent_oid))

        self.__parents[oid] = parent_oid

        if parent_oid not in self.__childs:
            self.__childs[parent_oid] = []
        self.__childs[parent_oid].append(oid)

        root_oid = self.__set_root(oid)
        # Create posterity list, for fast update on whole branches.
        # Update speed seems to be much more critical than create, so
        # this index should significantly increase branch update time
        self.__update_root_posterity(oid, root_oid)

    def list_roots(self):
        """ Get ids of all roots objects."""
        return [obj[STORAGE_ID_KEY] for obj in self.__data.values()
                if not self.__parents.get(obj[STORAGE_ID_KEY])]

    def get(self, oid):
        """ Extracting the object from the database.

        :param oid: object identifier
        :return: dictoinry with object or None
        """
        data = self.__data.get(oid)

        # adding info about child and parent to result
        if data:
            data[STORAGE_CHILDS_KEY] = self.__childs.get(oid)
            parents_oid = self.__parents.get(oid)
            if parents_oid:
                data[STORAGE_PARENT_KEY] = parents_oid
                data[STORAGE_ROOT_KEY] = self.__roots_of_objects.get(oid)
            else:
                data[STORAGE_POSTERITY_KEY] = \
                    self.__roots_posterity.get(oid)

        return data

    def find(self, ids: list):
        """ Looking for more objects by list.

        :param ids: identifiers of objects as a list
        :return: dictionary of object dictionaries. Keys are identifiers.
        """
        return {oid: obj for oid, obj in zip(ids, map(self.get, ids)) if obj}

    def create(self, **kwargs):
        """ Create new object.

        You have to give ID in __id key. If you miss it, OID will be generated.
        You can add __parent attribute, to determinate parent object of this
        object,

        :param kwargs: object fields.
        :return: OID of the just created object
        """
        oid = kwargs.get(STORAGE_ID_KEY, uuid.uuid4())
        if oid in self.__data:
            raise AlreadyExistsException(
                'Object {id} already exists'.format(id=oid))

        if STORAGE_PARENT_KEY in kwargs:
            parent_oid = kwargs[STORAGE_PARENT_KEY]
            # check if parent object exists
            self.__set_parent(oid, parent_oid)

            # to not store parent in two place - easier to maintain
            del kwargs[STORAGE_PARENT_KEY]

        self.__data[oid] = kwargs
        return oid

    def update(self, oid, **kwargs):
        """ Update object.

        If you give __parent attribute, and it will be another than old parent,
        it will move object to new parent and delete it from old one.

        :param oid: object identifier.
        """
        if oid not in self.__data:
            raise NotExistsException(
                'Object {id} can\'t be updated - not exists in db'.format(
                    id=oid))

        old_parent_oid = self.__parents.get(oid)
        parent_oid = kwargs.get(STORAGE_PARENT_KEY)
        if parent_oid and old_parent_oid != parent_oid:
            old_root_oid = self.__roots_of_objects.get(oid)
            self.__set_parent(oid, parent_oid)

            # to not store parent in two place - easier to maintain
            del kwargs[STORAGE_PARENT_KEY]
            self.__roots_posterity[old_root_oid].remove(oid)
            self.__childs[old_parent_oid].remove(oid)

        self.__data[oid].update(kwargs)

    def delete(self, oid):
        """ Delete object.

        :param oid: object identifier.
        """
        if oid not in self.__data:
            raise NotExistsException(
                'Object {id} can\'t be remove - not exists in db'.format(
                    id=oid))

        parent_oid = self.__parents.get(oid)
        root_oid = self.__roots_of_objects.get(oid)
        if root_oid:
            self.__roots_posterity[root_oid].remove(oid)
            del self.__roots_of_objects[oid]
        if parent_oid:
            self.__childs[parent_oid].remove(oid)
            del self.__parents[oid]
        del self.__data[oid]
