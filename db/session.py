import logging
import threading
from queue import Queue

from db.const import STORAGE_ROOT_KEY, ACTION_DELETE, STORAGE_POSTERITY_KEY, \
    ACTION_UPDATE, EVENT_TYPE_UPDATE, EVENT_TYPE_CREATE, EVENT_TYPE_DELETE, \
    EVENT_TYPE_RETRIEVE, STORAGE_CHILDS_KEY
from db.exceptions import DeleteException, LockIsAlreadySetException, \
    LockException, UpdateException, AddingEventException
from db.lock import Locks
from db.storage import Storage


logger = logging.getLogger(__name__)


class DBSession:
    """ DB Session class.

    It will bu used as frontend of DB. It's also responsible for manage locks
    to objects during DB access.
    """

    def __init__(self, locks: Locks, storage: Storage, threads=1):
        self.storage = Storage(storage)
        self.locks = locks
        self.threading = threads > 1
        self.queue = Queue()
        self.events = {
            EVENT_TYPE_UPDATE: [],
            EVENT_TYPE_CREATE: [],
            EVENT_TYPE_DELETE: [],
            EVENT_TYPE_RETRIEVE: [],
        }

        if self.threading:
            for i in range(threads):
                t = threading.Thread(target=self.__serve_thread)
                t.daemon = True
                t.start()

    def __serve_thread(self):
        while True:
            action = self.queue.get()
            if action['action'] == ACTION_DELETE:
                self.__delete(action['oid'])
            elif action['action'] == ACTION_UPDATE:
                self.__update(
                    action['oid'],
                    _populate=action['populate'],
                    **action['data'])

    def __get_root_oid(self, oid):
        obj = self.storage.get(oid)
        # if no STORAGE_ROOT_KEY, thaths mean we are in root object
        return obj.get(STORAGE_ROOT_KEY, oid)


    def __check_lock_tree(self, oid):
        """ Check tree lock.

        :param oid: object identifier
        :return: boolean
        """
        root_oid = self.__get_root_oid(oid)
        return self.locks.check(root_oid)

    def __lock_tree(self, oid):
        """ Lock whole tree.

        For prevent race in threads, we always lock root object and
        we recognize it as lock for all childs. In our situation is seems
        to be right solution, as we almost always update whole tree.

        :param oid: object identifier
        """
        root_oid = self.__get_root_oid(oid)
        self.locks.set(root_oid)
        return root_oid

    def __unlock_tree(self, oid, force=False):
        """ Lock whole tree.

        For prevent race in threads, we always lock root object and
        we recognize it as lock for all childs. In our situation is seems
        to be right solution, as we almost always update whole tree

        :param oid: object identifier
        """
        if not force:
            root_oid = self.__get_root_oid(oid)
        else:
            root_oid = oid
        self.locks.remove(root_oid)

    def __create_action(self, oid, action, populate=None, **kwargs):
        action = {'action': action, 'oid': oid}
        if populate:
            action['populate'] = populate
        action.update(kwargs)
        return action

    def list_roots(self):
        """ Get ids of all roots objects."""
        return self.storage.list_roots()

    def get(self, oid):
        """ Extracting the object from the database.

        :param oid: object identifier
        :return: dictoinry with object or None
        """
        result = self.storage.get(oid)
        self.call_events(EVENT_TYPE_RETRIEVE, oid=oid)
        return result

    def find(self, ids):
        """ Looking for more objects by list.

        :param ids: identifiers of objects as a list
        :return: dictionary of object dictionaries. Keys are identifiers.
        """
        result = self.storage.find(ids)
        self.call_events(EVENT_TYPE_RETRIEVE, ids=ids)
        return result

    def create(self, **kwargs):
        """ Create new object.

        You have to give ID in __id key. If you miss it, OID will be generated.
        You can add __parent attribute, to determinate parent object of this
        object,

        :param kwargs: object fields.
        :return: OID of the just created object
        """
        result = self.storage.create(**kwargs)
        self.call_events(EVENT_TYPE_CREATE, **kwargs)
        return result

    def __task_done(self, _ignore_done=False):
        if self.threading and not _ignore_done:
            self.queue.task_done()

    def __update(self, oid, _ignore_done=False, _populate=None, **kwargs):
        """ Update object. """
        if callable(_populate):
            data = self.storage.get(oid)
            values = _populate(data, kwargs)
            if not values:
                self.__task_done(_ignore_done)
                return
            if isinstance(values, dict):
                kwargs.update(values)

        self.storage.update(oid, **kwargs)
        self.call_events(EVENT_TYPE_UPDATE, oid=oid, **kwargs)
        self.__task_done(_ignore_done)

    def update(self, oid, populate=False, **kwargs):
        """ Update object.

        If you give __parent attribute, and it will be another than old parent,
        it will move object to new parent and delete it from old one.

        :param oid: object identifier.
        :param populate: if True, set this same values for all sub-objects
            in object's tree
        """
        try:
            if self.__check_lock_tree(oid):
                raise UpdateException('Object is locked.')
        except AttributeError:
            raise UpdateException('Object not exists.')

        if not populate:
            return self.__update(oid, _ignore_done=True, **kwargs)

        root_oid = self.__get_root_oid(oid)
        obj = self.storage.get(root_oid)
        posterity_objs = obj[STORAGE_POSTERITY_KEY]

        if self.threading:
            self.queue.put(
                self.__create_action(
                    root_oid, ACTION_UPDATE, populate=populate,
                    data=kwargs))
        else:
            self.__update(root_oid, _populate=populate, **kwargs)

        for posterity_obj in posterity_objs:
            if self.threading:
                self.queue.put(
                    self.__create_action(
                        posterity_obj, ACTION_UPDATE, populate=populate,
                        data=kwargs))
            else:
                self.__update(posterity_obj, _populate=populate, **kwargs)

        if self.threading:
            self.queue.join()

    def __delete(self, oid):
        """ Delete object. """
        self.storage.delete(oid)
        self.call_events(EVENT_TYPE_DELETE, oid=oid)
        self.__task_done()

    def __generate_delete_oids(self, oid):
        obj = self.storage.get(oid)
        children = obj[STORAGE_CHILDS_KEY]
        oids = [oid]
        if children:
            for oid in children:
                oids.extend(self.__generate_delete_oids(oid))
        return oids

    def delete(self, oid):
        """ Delete object.

        Remove object with all his children.

        :param oid: object identifier.
        """

        try:
            if self.__check_lock_tree(oid):
                raise DeleteException('Object is locked.')

            root_oid = self.__lock_tree(oid)
        except (LockException, AttributeError):
            raise DeleteException(
                'Can\'t set lock for delete object {id}'.format(id=id))

        # we have to remove this object, and all child objects
        action = self.__create_action(oid, ACTION_DELETE)
        obj = self.storage.get(oid)
        children = obj[STORAGE_CHILDS_KEY][:]
        for child_oid in children:
            if self.threading:
                child_action = self.__create_action(child_oid, ACTION_DELETE)
                self.queue.put(child_action)
            else:
                self.__delete(child_oid)

        if self.threading:
            self.queue.put(action)
            self.queue.join()
        else:
            self.__delete(oid)

        self.__unlock_tree(root_oid, force=True)

    def add_event(self, etype, method):
        """ Add new event.

        :param etype: event type: EVENT_TYPE_UPDATE, EVENT_TYPE_CREATE,
            EVENT_TYPE_DELETE, EVENT_TYPE_RETRIEVE
        :param method: method to call on event
        """
        if etype not in self.events:
            raise AddingEventException('Incorrect event type')

        self.events[etype].append(method)

    def call_events(self, etype, **kwargs):
        """ Call all event of given type

        :param etype:  event type: EVENT_TYPE_UPDATE, EVENT_TYPE_CREATE,
            EVENT_TYPE_DELETE, EVENT_TYPE_RETRIEVE
        :param kwargs: attributes for event
        """
        if etype not in self.events:
            raise AddingEventException('Incorrect event type')

        for event in self.events[etype]:
            event(**kwargs)
