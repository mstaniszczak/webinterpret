from db.lock import Locks, ListLocks
from db.storage import Storage, DictStorage

DB_STORAGE = Storage(DictStorage())
DB_LOCKS = Locks(ListLocks())
