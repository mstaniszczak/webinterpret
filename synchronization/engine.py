import json
import logging

from db.const import EVENT_TYPE_UPDATE, STORAGE_POSTERITY_KEY, \
    STORAGE_PARENT_KEY, STORAGE_ID_KEY
from db.session import DBSession
from files.input import InputData
from synchronization.settings import DB_LOCKS, DB_STORAGE


logger = logging.getLogger(__name__)


class Engine:
    def __init__(self, input_filename, output_filename, threads=1):
        self.session = DBSession(
            locks=DB_LOCKS,
            storage=DB_STORAGE,
            threads=threads)
        self.input_data = InputData(input_filename)
        self.output_file = open(output_filename, 'wt')
        self.output_data = []
        self.session.add_event(EVENT_TYPE_UPDATE, self._update_event)

    def _update_event(self, oid, stock=None, ended=None):
        """ This time we are using it only for log. """
        logger.info(
            "Update product {oid} - stock: {stock}, ended: {ended}",
            oid, stock, ended)

    def _populate(self, oid):
        """ Pupulate method - here we are using it as a filter. """
        def populate(data, new_data):
            if data['ended']:
                return False

            ended = 'ended' in new_data
            new_stock = new_data.get('stock')

            # ProductEnded
            # 1. if ended on child product, we not populate
            if ended and STORAGE_PARENT_KEY in data:
                return False

            # 2. in other case we end all product in tree and add logs
            if oid != data[STORAGE_ID_KEY]:
                if ended:
                    self.output_data.append({
                        'type': 'EndProduct',
                        'id': data[STORAGE_ID_KEY],
                    })
                elif new_stock:
                    self.output_data.append({
                        'type': 'UpdateProduct',
                        'id': data[STORAGE_ID_KEY],
                        'stock': new_stock,
                    })

            # UpdateProduct with stock == 0
            if new_stock is not None and new_stock == 0 and \
                    oid != data[STORAGE_ID_KEY]:
                self.output_data.append({
                    'type': 'EndProduct',
                    'id': data[STORAGE_ID_KEY],
                })
                # if it's not product which is pinged to close, bo one of
                # related products, we this closing product, but keep old stock
                return {'ended': True, 'stock': data['stock']}
            return True

        return populate

    def run(self):
        """ Execute parsing. """
        for data in self.input_data:
            oid = data['id']
            etype = data['type']
            obj = self.session.get(oid)
            if obj and obj['ended']:
                continue

            if etype == 'ProductCreated':
                kwargs = dict(
                    __id=data['id'],
                    stock=data['stock'],
                    ended=False,
                )
                if data['parent_id']:
                    kwargs['__parent'] = data['parent_id']
                try:
                    self.session.create(**kwargs)
                except Exception:
                    logger.exception('Problem with creating object.')
                    continue
            elif etype == 'ProductUpdated':
                if not obj:
                    continue

                stock = data['stock']

                self.session.update(
                    oid,
                    populate=self._populate(oid),
                    stock=stock)
            elif etype == 'ProductEnded':
                if not obj:
                    continue
                self.session.update(
                    oid,
                    populate=self._populate(oid),
                    ended=True)

        stocks = {}
        for root_oid in self.session.list_roots():
            root_data = self.session.get(root_oid)
            stocks[root_oid] = root_data['stock']

            for sub_object_oid in root_data[STORAGE_POSTERITY_KEY]:
                data = self.session.get(sub_object_oid)
                stocks[sub_object_oid] = data['stock']

        self.output_data.append({'stocks': stocks, 'type': 'StockSummary'})
        self.save_output()

    def save_output(self):
        for line in self.output_data:
            self.output_file.write("{line}\n".format(line=json.dumps(line)))
