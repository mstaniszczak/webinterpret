import unittest

import os

from files.input import InputData


class TestStorage(unittest.TestCase):
    def setUp(self):
        self.input = InputData("{path}/data/input.txt".format(
            path=os.path.dirname(__file__)))

    def test_iteration(self):
        data = []
        for line in self.input:
            data.append(line)

        timestamps = [d['timestamp'] for d in data]

        for ts in (125, 130, 131, 133, 136):
            self.assertNotIn(ts, timestamps)

        for ts in (123, 124, 126, 127, 128, 129, 132, 134, 135, 137):
            self.assertIn(ts, timestamps)
