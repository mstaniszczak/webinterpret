import json


class InputData:
    """ Class for serve input file. """

    def __init__(self, filename):
        """ Initialization.

        :param filename: path to file
        """
        self.file = open(filename)
        self.last_timestamp = None

    def __iter__(self):
        """ Mart `self` as iterator. """
        return self

    def __next__(self):
        """ Serve next line. """
        line = self.file.readline()
        if not line:
            self.file.close()
            raise StopIteration

        line = self.__parse_line(line)

        if line is None:
            line = self.__next__()
        return line

    def __validate_event(self, data):
        """ Validate event for every line of file.

        :param data: disct with parsed line
        :return: dict with line or None if line should be ignored
        """
        if 'type' not in data or \
                data['type'] not in ('ProductCreated', 'ProductUpdated',
                                     'ProductEnded'):
            return None

        if data['type'] == 'ProductCreated' and \
                not all(k in data for k in
                        ('id', 'stock', 'timestamp', 'parent_id')):
            return None
        elif data['type'] == 'ProductUpdated' and \
                not all(k in data for k in ('id', 'stock', 'timestamp')):
            return None
        elif data['type'] == 'ProductEnded' and \
                not all(k in data for k in ('id',)):
            return None

        if self.last_timestamp is None or \
                self.last_timestamp < data['timestamp']:
            self.last_timestamp = data['timestamp']
        elif self.last_timestamp > data['timestamp']:
            return None

        return data

    def __parse_line(self, line):
        """ Parse event lines.

        :param line: strign with line to parse
        :return: dict with line or None if line should be ignored
        """
        # remove comment
        if '//' in line:
            in_json = False
            in_string = False
            for i, c in enumerate(line):
                if not in_json and c == '{':
                   in_json = True
                elif in_json and not in_string and c == '"':
                    in_string = True
                elif in_json and in_string and c == '"':
                    in_string = False
                elif in_json and in_string:
                    continue
                elif in_json and c == '}':
                    break

            line = line[:i+1]

        line = json.loads(line)

        # ignore incorrect lines
        line = self.__validate_event(line)
        return line
